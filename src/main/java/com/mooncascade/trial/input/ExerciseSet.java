package com.mooncascade.trial.input;

public class ExerciseSet {
    private int kg;
    private int reps;

    public ExerciseSet(int kg, int reps) {
        this.kg = kg;
        this.reps = reps;
    }

    public int getKg() {
        return kg;
    }

    public int getReps() {
        return reps;
    }
}
