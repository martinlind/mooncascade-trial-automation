package com.mooncascade.trial.input;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Encapsulates input data for the tests
 */
public class TestInput {
    private String exerciseName;
    private Map<String, List<ExerciseSet>> exercises;

    public void setExerciseName(String name) {
        this.exercises = new HashMap<>();
        this.exerciseName = name;
    }

    public void addExercise(ExerciseSet exerciseSet) {
        exercises.computeIfAbsent(exerciseName, k -> new ArrayList<>());
        exercises.get(exerciseName).add(exerciseSet);
    }

    public Map<String, List<ExerciseSet>> getExercises() {
        return exercises;
    }
}
