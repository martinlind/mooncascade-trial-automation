package com.mooncascade.trial.config;

import com.mooncascade.trial.input.ExerciseSet;
import com.mooncascade.trial.input.TestInput;
import cucumber.api.Scenario;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Map;

public class WebBrowser {
    private static final String WORKOUT_URL = "/workout";
    private static final String DELETE_WORKOUT_XPATH = "//a[@data-form='.delete-workout']";

    private final TestConfig config;
    private final ChromeDriver driver;
    private final TestInput input;

    private String lastWorkoutUrl;

    public WebBrowser (TestConfig config, TestInput input) {
        this.config = config;
        this.input = input;
        System.setProperty("webdriver.chrome.driver", config.getChromeDriver());
        ChromeOptions options = new ChromeOptions();
        driver = new ChromeDriver(options);
    }

    // -- Login - start ---

    public void logIn() {
        goToHomePage();
        goToLoginLink();

        waitUntilLoginWindowOpen();
        enterLoginCredentials();
        clickLoginButton();
    }

    private void goToHomePage() {
        driver.get(config.getUrl());
    }

    private void goToLoginLink() {
        driver.findElement(By.xpath("//a[@href='#login-front']")).click();
    }

    private void waitUntilLoginWindowOpen() {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(By.name("email")));
    }

    private void enterLoginCredentials() {
        driver.findElement(By.name("email")).sendKeys(config.getEmail());
        driver.findElement(By.name("password")).sendKeys(config.getPassword());
    }

    private void clickLoginButton() {
        // TODO: Is there a better way to get the button?
        driver.findElement(By.xpath("//div[@class='col-sm-10 col-sm-offset-1']")).click();
    }

    // -- Login - end ---

    public void verifyLoggedInSuccessfully() {
        if (!driver.findElements(By.xpath("//div[@class='alert alert-danger']")).isEmpty()) {
            throw new RuntimeException("Login failed, make sure the user has been created");
        }
    }

    public void close(Scenario scenario) {
        if (!scenario.isFailed()) {
            driver.close();
        }
    }

    // -- Adding workout - start ---

    public void startAddingWorkout() {
        driver.get(config.getUrl() + WORKOUT_URL);
        lastWorkoutUrl = driver.getCurrentUrl();
    }

    public void addNewExercises() {
        List<WebElement> exerciseNames = driver.findElements(By.name("exercise_name[]"));
        int iExercise = 0;
        for(Map.Entry<String, List<ExerciseSet>> entry : input.getExercises().entrySet()) {
            exerciseNames.get(iExercise).sendKeys(entry.getKey());

            int iSet = 0;
            for(ExerciseSet exs : entry.getValue()) {
                List<WebElement> weights = driver.findElements(By.name("weight[" + iExercise + "][]"));
                List<WebElement> reps = driver.findElements(By.name("reps[" + iExercise + "][]"));

                weights.get(iSet).sendKeys(Integer.toString(exs.getKg()));
                reps.get(iSet).sendKeys(Integer.toString(exs.getReps()));
                iSet++;
            }
            iExercise++;
        }
    }

    public void setTrainingBodyweight(int bodyweight) {
        driver.findElement(By.name("bodyweight")).sendKeys(Integer.toString(bodyweight));
    }

    public void setWorkoutNotes(String notes) {
        driver.findElement(By.name("notes")).sendKeys(notes);
    }

    public void saveTWorkout() {
        driver.findElement(By.xpath("//button[@class='btn btn-success']")).click();
    }

    public boolean verifyAddedWorkoutIsSaved() {
        driver.get(lastWorkoutUrl);
        return !(driver.findElements(By.xpath(DELETE_WORKOUT_XPATH)).isEmpty());
    }

    public void removeCreatedWorkout() {
        if(verifyAddedWorkoutIsSaved()) {
            driver.get(lastWorkoutUrl);
            driver.findElement(By.xpath("//a[@data-form='.delete-workout']")).click();
            driver.switchTo().alert().accept();
        }
    }

    // -- Adding workout  - end ---

}
