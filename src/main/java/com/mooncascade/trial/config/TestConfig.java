package com.mooncascade.trial.config;

import java.io.IOException;
import java.util.Properties;

/**
 * Configuration common to all of the tests.
 */
public class TestConfig {

    private Properties props;

    public TestConfig() {
        props = new Properties();

        try {
            props.load(this.getClass().getResourceAsStream("/application.properties"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * @return Chrome driver location in the file system.
     */
    String getChromeDriver() {
        return getRequiredProperty("config.chromeDriver");
    }

    String getUrl() {
        return getRequiredProperty("app.url");
    }

    String getEmail() {
        return getRequiredProperty("app.email");
    }

    String getPassword() {
        return getRequiredProperty("app.password");
    }

    private String getRequiredProperty(String propertyKey) {
        String propertyValue = props.getProperty(propertyKey);
        if (propertyValue == null) {
            throw new RuntimeException(propertyKey + " is required, make sure it's present in the properties file!");
        }

        return propertyValue;
    }
}
