package com.mooncascade.trial;

import com.mooncascade.trial.config.TestConfig;
import com.mooncascade.trial.config.WebBrowser;
import com.mooncascade.trial.input.ExerciseSet;
import com.mooncascade.trial.input.TestInput;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;

import java.util.List;

import static junit.framework.TestCase.assertTrue;

public class Workout {

    private WebBrowser browser;
    private TestInput input;

    public Workout() {
        input = new TestInput();
        browser = new WebBrowser(new TestConfig(), input);
    }

    @Given("ensure test user exists")
    public void ensureTestUserExists() {
        browser.logIn();
        browser.verifyLoggedInSuccessfully();
    }

    @Given("user moves to the workout adding window")
    public void userMovesToTheWorkoutAddingWindow()  {
        browser.startAddingWorkout();
    }

    @Given("set \"([^\"]*)\" as a current exercise")
    public void setNameOfTheCurrentExercise(String name) {
        input.setExerciseName(name);
    }

    @Given("set the following sets - kg, reps - for the exercise:")
    public void addSetsToTheCurrentExercise(DataTable exerciseInfo) {
        for (List item : exerciseInfo.asLists()) {
            input.addExercise(new ExerciseSet(
                    Integer.parseInt(item.get(0).toString()), Integer.parseInt(item.get(1).toString())));
        }
    }

    @When("fill in the form with the exercise data")
    public void fillInTheNewExerciseFormWithExerciseData() {
        browser.addNewExercises();
    }

    @When("set the bodyweight to {int} kg")
    public void fillInTheBodyweightOnNewWorkoutForm(Integer weight) {
        browser.setTrainingBodyweight(weight);
    }

    @When("write \"([^\"]*)\" as workout notes")
    public void addNotesToTheTraining(String notes) {
        browser.setWorkoutNotes(notes);
    }

    @When("save the workout")
    public void saveWorkout() {
        browser.saveTWorkout();
    }

    @Then("verify that the added workout has been saved")
    public void verifyAddedWorkoutIsSaved() {
        assertTrue(browser.verifyAddedWorkoutIsSaved());
    }

    @After
    public void tearDown(Scenario scenario) {
        browser.removeCreatedWorkout();
        browser.close(scenario);
    }
}
