Feature: workout
  Background:
    Given ensure test user exists

  Scenario: create a workout
    Given user moves to the workout adding window
    And set "Kükk" as a current exercise
    And set the following sets - kg, reps - for the exercise:
      | 110 | 3 |
      | 120 | 3 |
      | 130 | 3 |
    When fill in the form with the exercise data
    And set the bodyweight to 77 kg
    And write "My test notes" as workout notes
    And save the workout
    Then verify that the added workout has been saved
