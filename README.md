# mooncascade-trial-automation

Exercise 4 - test automation

4) Automated tests

Application: Gymwolf web app
Staging URL: https://www.gymwolf.com/staging/
As mentioned, this is not a production environment data and users can be freely created/generated.

Task: Write 2-3 automated tests - Functionality can be chosen freely but at least one test should include data input (posting via form or similar) and validation. 

Some guidelines:

* Code should be reusable, readable and uploaded to a repository (Bitbucket, GitLab, any other)
* Selenium, Gradle/Maven and JUnit/TestNG are preferred but not mandatory

## Setting up the environment

Set up the **configuration**.

```
cp src/main/resources/application.properties.sample src/main/resources/application.properties
```

Find **Chrome driver** and set it up under the key `webdriver.chrome.driver`, on UNIX-like systems one way to find it out is using:

```
whereis chromedriver
```
## Current environment (where the solution was tested)

**Java** version:

```
$java -version
openjdk version "11.0.1" 2018-10-16
OpenJDK Runtime Environment (build 11.0.1+13-Ubuntu-3ubuntu118.04ppa1)
OpenJDK 64-Bit Server VM (build 11.0.1+13-Ubuntu-3ubuntu118.04ppa1, mixed mode, sharing)
```

**Gradle** version: `5.2.1`

**IntelliJ IDEA (CE)** version: `2018.3.4`

**OS kernel** version: `Linux 4.15.0-45-generic amd64`

## Running Cucumber tests:

Using **Gradle** from the project root folder (runs all of the tests):

```
gradle cucumber
```

Tests can also be run from IDE, for example, with **IntelliJ IDEA** it can be done in the following way:

* set the cursor to the feature, scenario or folder to interest;
* hit **Ctrl + Shift + F10** to run the respective tests;
* wait until the tests finish.

From the IDE, single scenarios can be easily run. Keep in mind that IDE may need its specific **Cucumber plugin**.
